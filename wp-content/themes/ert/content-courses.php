<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>
<div class="span3 module_cont module_iconboxes <?php the_slug(); ?>">
  <div class="shortcode_iconbox item">
    <?php the_title( '<h4>', '</h4>' ); ?>
    <?php print the_excerpt(); ?>
		<a href="<?php print get_permalink(); ?>" class="shortcode_button btn_small btn_type1">Read more</a>
  </div>
</div>