<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>

		<?php
			while (have_rows('modules')) : the_row();
				include THEME_DIRECTORY . '/includes/modules/' . str_replace('_', '-', get_row_layout()) . '.php';
			endwhile;
		?>