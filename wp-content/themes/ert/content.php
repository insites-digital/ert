<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		ert_post_thumbnail();
	?>
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="headInModule">', '</h1>' );
			else :
				the_title( '<h2 class="headInModule">', '</h2>' );
			endif;
		?>

	<div class="entry-content">
		<?php
		?>
		<?php
			if ( is_single() ) :
				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading %s', 'ert' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				) );
			else :
				the_excerpt();
			endif;
		?>
	</div><!-- .entry-content -->
	<div class="course-bookings promoblock">
		<?php
		if( have_rows('purchase') ):
		while( have_rows('purchase') ): the_row(); 
			$bookText = get_sub_field('book_text');
			$bookPrice = get_sub_field('course_price');
			$bookLink = get_sub_field('booking_url');
		?>
		<?php if( $bookText ): ?>
			<p class="promo-para">
			<?php print $bookText; ?> - &pound;<?php print $bookPrice; ?>
			</p>
		<?php endif; ?>
		
	  <div class="shortcode_promoblock ">
	    <div class="row-fluid">
	      <div class="span3 promo_button_block">
	       	<a href="<?php print $bookLink; ?>" class="promo_button">Book now!</a>
	      </div>
	    </div>
	  </div>
	
		<?php 
		endwhile; 
		endif;
		?>
	</div>
</article><!-- #post-## -->
