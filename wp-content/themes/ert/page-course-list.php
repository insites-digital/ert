<?php
/**
 * The template for displaying all single posts and attachments
 * Template Name: Course List
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */

get_header(); ?>
    <div id="primary" class="content-area content_wrapper">
		<main id="main" class="site-main" role="main">
    	<div class="container">
        <div class="content_block no-sidebar row">
          <div class="fl-container span12">    
            <div class="row-fluid">
							<?php 
							$query = new WP_Query( array('post_type' => 'course', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' =>'menu_order'));
							while ( $query->have_posts() ) : $query->the_post();
								get_template_part( 'content', 'list' );
							endwhile; 
							wp_reset_postdata(); ?>	            
            </div><!-- .row-fluid -->
          </div><!-- .contentarea -->
        </div>
      </div>
      <div class="clear"><!-- ClearFix --></div>
    </div><!-- .fl-container -->
    <div class="clear"><!-- ClearFix --></div>
  </div>
  </div><!-- .container -->
  </main>
</div><!-- .content_wrapper -->
<?php get_footer(); ?>
