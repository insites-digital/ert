<div class="promo-strip module">
  <div class="container">
	  <div class="promo-headline"><?php the_sub_field("promo_headline"); ?></div>    
	  <p><?php the_sub_field("promo_text"); ?></p>
  </div>
</div>
