<section class="section checkerboard clearfix module">
	<div class="module-title">
		<h3>The ERT Journey</h3>
	</div>
	
	<?php
	if( have_rows('checkerboard_row') ):
		$step = 0;
    while ( have_rows('checkerboard_row') ) : the_row();
    $step++;
	?>
	<div class="checkerboard-row">
    <div class="checkerboard-col checkerboard-col-text">
      <div class="checkerboard-col-text-inner">
        <h3><?php the_sub_field("row_title"); ?></h3>	
        <p><?php the_sub_field("row_body"); ?></p>
        <a href="<?php the_sub_field("row_cta_url"); ?>" class="checkerboard-btn">
        	<span class="checkerboard-btn-text"><?php the_sub_field("row_cta"); ?></span>
        </a>
        <span class="checkerboard-tag">Step <?php print $step; ?></span>
      </div>
    </div>
    <div class="checkerboard-col checkerboard-col-image" style="background-image: url(<?php the_sub_field("row_image"); ?>)">
    </div>
	</div>
	<?php
    endwhile;
	else :
	
	endif;
	?>	    
  <div class="checkboard-cta">
    <a href="<?php the_sub_field("checkerboard_all_URL"); ?>" class="" title="">
    	<span class="checkerboard-btn-text"><?php the_sub_field("checkerboard_all_text"); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></span>
    </a>
  </div>
</section>
