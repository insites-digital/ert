<div class="container module">
	<div class="row fluid">
		<div class="span12 lead">
			<div class="module-title">
				<h3><?php the_sub_field('text_block_title'); ?></h3>
			</div>
		  <?php the_sub_field("body_copy"); ?>
	  </div>
	</div>	
</div>