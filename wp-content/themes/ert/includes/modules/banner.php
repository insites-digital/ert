<div class="hero-banner" style="background-image: url(<?php the_sub_field("banner_image"); ?>);">
  <div class="container">
  	<div class="hero-top">
	  	<?php the_sub_field("banner_top"); ?>
  	</div>
  	<div class="hero-title">
	  	<h2><?php the_sub_field("banner_headline"); ?></h2>
			<h3><?php the_sub_field("banner_sub_heading"); ?></h3>
  	</div>
  	<div class="hero-cta">
			<?php the_sub_field("banner_button_text"); ?>
	  	<a href="<?php the_sub_field("banner_button_target"); ?>" title="View our motorbike training courses">Our Courses</a>
  	</div>
  </div>
</div>
