<div class="container">
	<div class="row-fluid">
	  <aside class="brand-bar clearfix module">
	    <h1 class="brand-bar-title"><?php the_sub_field("logo_bar_title"); ?></h1>
	    <h2 class="brand-bar-subtitle"><?php the_sub_field("logo_bar_subtitle"); ?></h2>    
	    <div class="brand-grid">
				<?php
				if( have_rows('logos') ):
			    while ( have_rows('logos') ) : the_row();
				?>
					<?php 
					$image = get_sub_field('logo_image');
					$url = $image['url'];
					$alt = $image['alt'];
					$width = $image['width'];
					$height = $image['height'];
					
					$ex_url = get_sub_field('logo_external_link');
					?>
			    <div class="brand-item">
				    <?php if($ex_url){ ?>
				    <a href="<?php print $ex_url; ?>" target="_blank">
				    <?php } ?>
					  	<img src="<?php print $url; ?>" alt="<?php print $alt; ?>" width="<?php print $width; ?>" height="<?php print $height; ?>" />
				    <?php if($ex_url){ ?>
				    </a>
				    <?php } ?>
			    </div>
				<?php
			    endwhile;
				else :
				
				endif;
				?>	    
	    </div>
	  </aside>
	</div>	
</div>