<section class="section map clearfix module">
	<div class="container">
		<div class="row fluid">
			<div class="module-title">
				<h1><?php the_sub_field('location_title'); ?></h1>
			</div>
			<div class="module-content">
				<div class="span4">
				<?php if( have_rows('addresses') ): ?>
					<?php while ( have_rows('addresses') ) : the_row(); ?>
					<div class="address-item" itemprop="branchOf" itemscope itemtype="http://schema.org/LocalBusiness">
						<h4 itemprop="name"><?php the_sub_field('address_title'); ?></h4>
						<?php $mapurl = get_sub_field('google_maps_business_url');?>
						<?php if($mapurl): ?>
						<link itemprop="hasMap" href="<?php the_sub_field('google_maps_business_url'); ?>">
						<?php endif; ?>
		        <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							<p><span itemprop="streetAddress"><?php the_sub_field('street_address'); ?></span><br>
	            <span itemprop="addressLocality"><?php the_sub_field('town_address'); ?></span>,<br> 
	            <span itemprop="addressRegion"><?php the_sub_field('region_address'); ?></span> <br>
	            <span itemprop="postalCode"><?php the_sub_field('postcode_address'); ?></span></p>
		        </div>
						<a href="<?php the_sub_field('address_target'); ?>">Find out more</a>
						<?php $geo = get_sub_field('google_map'); ?>
						<span itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
							<meta itemprop="latitude" content="<?php echo $geo['lat']; ?>" />
							<meta itemprop="longitude" content="<?php echo $geo['lng']; ?>" />
						</span>
					</div>
					<?php endwhile; ?>
				<?php endif; ?>
				</div>
			<?php if( have_rows('addresses') ): ?>
				<div class="acf-map span8">
				<?php while ( have_rows('addresses') ) : the_row(); 
					$location = get_sub_field('google_map');
					?>
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
						<h4><?php the_sub_field('address_title'); ?></h4>
					</div>
				<?php endwhile; ?>
				</div>
			<?php endif; ?>
			</div>
		</div>
	</div>
</section>