<div class="container">
	<div class="row-fluid">
		<div class="teaser-tiles module">
			<div class="span4 module_cont module_iconboxes">
			  <div class="shortcode_iconbox item">
				  <span class="ico"><i class="fa fa-<?php the_sub_field("tile01_icon"); ?>" aria-hidden="true"></i></span>
			    <h4><?php the_sub_field("tile01_title"); ?></h4>    
			    <p><?php the_sub_field("tile01_body"); ?></p>
			  </div>
			</div>
			<div class="span4 module_cont module_iconboxes">
			  <div class="shortcode_iconbox item">
				  <span class="ico"><i class="fa fa-<?php the_sub_field("tile02_icon"); ?>" aria-hidden="true"></i></span>
			    <h4><?php the_sub_field("tile02_title"); ?></h4>    
			    <p><?php the_sub_field("tile02_body"); ?></p>
			  </div>
			</div>
			<div class="span4 module_cont module_iconboxes">
			  <div class="shortcode_iconbox item">
				  <span class="ico"><i class="fa fa-<?php the_sub_field("tile03_icon"); ?>" aria-hidden="true"></i></span>
			    <h4><?php the_sub_field("tile03_title"); ?></h4>    
			    <p><?php the_sub_field("tile03_body"); ?></p>
			  </div>
			</div>
		</div>
	</div>
</div>