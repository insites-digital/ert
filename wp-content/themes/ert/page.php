<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */

get_header(); ?>
    <div id="primary" class="content-area content_wrapper">
		<main id="main" class="site-main" role="main">
    	<div class="container">
        <div class="content_block no-sidebar row">
          <div class="fl-container span12">    
            <div class="row-fluid">
							<?php
							// Start the loop.
							while ( have_posts() ) : the_post();
					
								/*
								 * Include the post format-specific template for the content. If you want to
								 * use this in a child theme, then include a file called called content-___.php
								 * (where ___ is the post format) and that will be used instead.
								 */
								get_template_part( 'content', 'page');
					
							// End the loop.
							endwhile;
							?>
            </div><!-- .row-fluid -->
          </div><!-- .contentarea -->
        </div>
      </div>
      <div class="clear"><!-- ClearFix --></div>
    </div><!-- .fl-container -->
    <div class="clear"><!-- ClearFix --></div>
  </div>
  </div><!-- .container -->
  </main>
</div><!-- .content_wrapper -->
<?php get_footer(); ?>
