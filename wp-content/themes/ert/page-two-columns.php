<?php
/**
 * The template for displaying all single posts and attachments
 * Template Name: Two Columns
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */

get_header(); ?>
    <div id="primary" class="content-area content_wrapper">
		<main id="main" class="site-main" role="main">
    	<div class="container">
        <div class="content_block no-sidebar row">
          <div class="fl-container span12">    
	        	<div class="row">
              <div class="posts-block span12">
                <div class="contentarea">
                  <hr class="light">
										<?php
										// Start the loop.
										while ( have_posts() ) : the_post();
											get_template_part( 'content', 'two-columns');
								
										// End the loop.
										endwhile;
										?>
                  <hr class="light">
                </div><!-- .contentarea -->
              </div>
	            </div>
	            <div class="clear"><!-- ClearFix --></div>
			    </div><!-- .fl-container -->
			  <div class="clear"><!-- ClearFix --></div>
	  </div>
  </div><!-- .container -->
  </main>
</div><!-- .content_wrapper -->
<?php get_footer(); ?>
