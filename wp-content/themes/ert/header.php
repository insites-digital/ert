<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script type='text/javascript'>
	//<![CDATA[ 
	function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }loadCSS("https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css");
	//]]> 
	</script>
	<script type="text/javascript">
	  WebFontConfig = {
	    google: { families: [ 'Open+Sans:300,400,600,700:latin' ] }
	  };
	  (function() {
	    var wf = document.createElement('script');
	    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
	      '://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
	    wf.type = 'text/javascript';
	    wf.async = 'true';
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(wf, s);
	  })(); 
  </script>

</head>

<body <?php body_class('user_img_layout'); ?>>
<div id="page" class="hfeed site">
	<header>
        <div class="header_wrapper container">
						<div itemscope itemtype="http://schema.org/Organization">
	            <a itemprop="url" href="http://www.ertbike.com/" class="logo"><img itemprop="logo" src="/wp-content/themes/ert/img/logo.png" alt="Exceptional Rider Training"  width="331" height="63" class="logo_def"></a>
						</div>
            <ul id="header-contact" class="clearfix">
	            <li><a href="tel:03331233023">03331 233 023 <i class="fa fa-phone" aria-hidden="true"></i></a></li>
	            <li><a href="/book">Book Online</a></li>
            </ul>
            <nav>
							<?php  
								wp_nav_menu( array(
								'menu' => 'Primary Menu',
								'container' => 'ul',
								'container_class' => 'menu',
								)); 
							?>
            </nav>
        </div><!-- .header_wrapper -->
      <div class="clear"></div>
	</header>


	<div id="content" class="site-content">
