<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row-fluid">
		<?php the_title( '<h1 class="headInModule">', '</h1>' ); ?>
	</div>
	<div class="row-fluid">
		<div class="span6 module_cont">
			<?php the_field('sub_content'); ?>
		</div><!--end of first column--->							
		<div class="span6 module_cont">
			<div class="entry-content">
				<?php	the_content();?>
			</div><!-- .entry-content -->
		</div><!--end of second column--->
	</div>									
</article><!-- #post-## -->
