<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>
<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
<div id="widget-area" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div>
<?php endif; ?>
<div class="row-fluid">
	<h4 class="headInModule clear">The courses we offer</h4>
</div>
<?php 
$i = 1;
print '<div class="row-fluid">';
$query = new WP_Query( array('post_type' => 'course', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' =>'menu_order'));
while ( $query->have_posts() ) : $query->the_post();
	get_template_part( 'content', 'courses' );
  if($i % 4 == 0) {
    print '</div><div class="row-fluid">';
  }
	$i++; 
endwhile; 
print '</div>';
wp_reset_postdata(); ?>