/*******************************************************************************/
/* GULP SETUP
/*******************************************************************************/
var gulp, concat, rename, uglify, cleanCSS, eslint,
SASS, babel, sourcemaps, watch, imagemin, pngquant, autoprefixer, plumber, liveReload, markdown, fileinclude, gutil, ftp, browserSync;
gulp             = require('gulp');
concat     = require('gulp-concat');
rename     = require('gulp-rename');
uglify     = require('gulp-uglify');
cleanCSS   = require('gulp-clean-css');
eslint       = require('gulp-eslint');
SASS         = require('gulp-sass');
babel        = require('gulp-babel');
sourcemaps = require('gulp-sourcemaps');
imagemin     = require('gulp-imagemin');
pngquant     = require('imagemin-pngquant');
autoprefixer = require('gulp-autoprefixer');	//autoprefixr
plumber = require('gulp-plumber');				//plumber
liveReload = require('gulp-livereload');		//livereload
markdown = require('gulp-markdown');			//markdown
fileinclude = require('gulp-file-include');		//fileinclude
gutil = require( 'gulp-util' );
ftp = require('vinyl-ftp');
browserSync = require('browser-sync').create();

			'js/dev/*.js',
			//don't include
			'!js/vendor/**/*.js',


watch      = require('gulp-watch');
const notifier = require('node-notifier');
gulp.task('concat', function() {
    gulp.src(['js/dev/*.js'])
        .pipe(concat('scripts.js'))
        .on('error', swallowError)
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js/dist'))
		    .pipe(browserSync.reload({ stream: true}));
    gulp.src('js/vendor/*.js')
        .pipe(concat('plugins.js'))
        .on('error', swallowError)
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js/dist'))
		    .pipe(browserSync.reload({ stream: true}));
});
gulp.task('scss', function() {
    gulp.src('sass/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(SASS({outputStyle: 'compressed'}))
        .on('error', swallowError)
				.pipe(autoprefixer({
				  browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
				}))
        .pipe(rename('main.min.css'))
        .on('error', swallowError)
        .pipe(sourcemaps.write('maps'))
        .on('error', swallowError)
        .pipe(gulp.dest('css/'))
		    .pipe(browserSync.reload({ stream: true}));
});

gulp.task( 'deploy', function () {
 
	var conn = ftp.create( {
	    host:     '185.17.180.119',
	    user:     'ertstaging',
	    password: 'WmiMep9AzXhM3MLQ',
	} );
	
	var globs = [
	    '**',
	    '!node_modules',
	    '!sass',
	    '!node_modules/**',
	    '!sass/**'
	];
	
	return gulp.src( globs, { base: '.', buffer: false } )
	  .pipe( conn.newer( 'public_html/wp-content/themes/ert' ) )
	  .pipe( conn.dest( 'public_html/wp-content/themes/ert' ) );
} );


gulp.task('imagemin', function() {
    gulp.src('img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .on('error', swallowError)
        .pipe(gulp.dest('img/'));
});

gulp.task('serve', ['scss'], function() {

    browserSync.init({
        open: 'external',
        host: 'ertbike.dev',
        proxy: 'ertbike.dev',
    });
});


gulp.task('watch', function() {
    gulp.watch(['js/dev/*.js', 'js/vendor/*.js', 'css/*.css'], ['concat']).on('change', browserSync.reload);
    gulp.watch('sass/**/*.scss', ['scss']).on('change', browserSync.reload);
});
gulp.task('default', ['concat', 'scss', 'watch', 'serve', 'deploy']);
function swallowError(error) {
    console.log(error.toString());
    try {
        notifier.notify({
            'title': 'Error:',
          'message': error.formatted.toString().trim().replace(/Error:/g, '')
        });
    } catch (e) {
        notifier.notify({
            'title': 'Error:',
          'message': error.toString()
        });
    }
    this.emit('end');
}
