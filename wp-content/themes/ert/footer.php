<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */
?>

	</div><!-- .site-content -->

    <footer>
    	<div class="footer_wrapper container">
	    	
	    <div class="footer-info clearfix row-fluid">
		    <div class="span3">
			   	<h5>Courses we offer</h5>
			   	<ul>
						<?php 
						$query = new WP_Query( array('post_type' => 'course', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' =>'menu_order'));
						while ( $query->have_posts() ) : $query->the_post(); ?>
				   	<li>
							<a href="<?php print get_permalink(); ?>"><?php the_title(); ?></a>
				   	</li>
						<?php endwhile; 
						wp_reset_postdata(); ?>	            
			   	</ul>
		    </div>
		    <div class="span3">
			   	<h5>Coverage</h5>
			   	<ul>
				   	<li><a href="https://ertbike.com/motorcycle-training-stalbans/">St Albans</a></li>
				   	<li><a href="https://ertbike.com/motorcycle-training-hemel-hempstead/">Hemel Hempstead</a></li>
				   	<li><a href="https://ertbike.com/harpenden-motorcycle-training/">Harpenden</a></li>
				   	<li><a href="https://ertbike.com/motorcycle-training-luton/">Luton</a></li>
				   	<li><a href="https://ertbike.com/motorcycle-training-stevenage/">Stevenage</a></li>
				   	<li><a href="https://ertbike.com/motorcycle-training-watford/">Watford</a></li>
				   	<li><a href="https://ertbike.com/motorcycle-training-herts/">Hertfordshire</a></li>
				   	<li><a href="https://ertbike.com/motorcycle-training-beds/">Bedfordshire</a></li>
				   	<li>Buckinghamshire</li>
			   	</ul>
		    </div>
		    <div class="span3">
			   	<h5>Our Location</h5>
			   	<p>Exceptional Rider Training<br>54 Sandpit Lane, St Albans,<br>Hertfordshire,<br>AL1 4BW</p>
		    </div>
		    <div class="span3">
			   	<h5>Contact Info</h5>
			   	<p>Mobile Friendly: <a href="tel:03331233023">03331 233 023</a><br>Landline Friendly: <a href="tel:08001123023">08001 123 023<br><a href="mailto:enquiries@ertbike.com">enquiries@ertbike.com</a></p>
				  <div class="footer_tools">
				    <ul class="footer_socials main_socials">
				        <li><a href="https://www.facebook.com/ERTbike/?fref=ts" class="ico_social-facebook" title="Facebook">/ERTbike</a></li>
				    </ul>            
				    <div class="clear"></div>                
				  </div>
		    </div>
	    </div>
	    	
	    	
          <div class="copyright">&copy; 2017 ERT LTD. All Rights Reserved.</div>
        </div>
	</footer>
<?php wp_footer(); ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-49610081-1', 'ertbike.com');
	  ga('send', 'pageview');
	</script>
</body>
</html>
