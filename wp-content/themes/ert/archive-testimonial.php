<?php
/**
 * The template for displaying all single posts and attachments
 * Template Name: Course List
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */

get_header(); ?>

<style>

.c-testimonial-grid .c-testimonial-content,
.c-testimonial-grid .review-item {
  padding: 1.5rem;
  flex-basis: 100%;
  margin-bottom: 10px;
  width: 100%;
  position: relative;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -webkit-transition: -webkit-box-shadow 0.2s ease-in-out;
  transition: box-shadow 0.2s ease-in-out;
  background: #fff;
  border-radius: 2px;
  display: block;
  padding: 30px 30px 60px 30px;
  color: #848484;
}

.c-band--testimonials{
    margin-top:150px;
    margin-bottom:150px;
}

.c-band--testimonials .promo-para{
    text-transform: uppercase;
    font-size:65px;
}

.c-testimonial-grid__item {
    margin: 10px;
    text-align :left;
}
p.promo-follow {
    font-size: 25px;
    margin: -10px 0 100px 0;
}
.c-testimonial-content__name {
    color: #002A41;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 20px;
}
.stars {
    color: #ffd102;
    padding: 10px 0;
    font-size: 29px;
}
.c-testimonial-content__time{
  padding-top:5px;
}
.c-testimonial-content__text {
    font-weight: lighter;
    padding: 12px 0px;
}
.readmore{
  color: #002A41;
  text-decoration: underline;
}

.readmore:hover{
  color:#002A42;
}

.call-to-action-testimonials{
  margin:100px 0;
}

.call-to-action-testimonials h3{
  color: #002A41;
  font-size:30px;
  font-weight:400;
  margin: 10px 0 50px 0;
}





.call-to-action-testimonials h3 strong{
  font-weight: bolder;
}

.call-to-action-testimonials .button{
  background: #ffd102;
  color: #002a41 !important;
  border-radius:5px;
  padding:20px 100px;
  margin-top:30px;
  text-transform: uppercase;
  font-weight: 800;
}
.button:hover {
    background: #cca604;
}
.c-testimonial h3.gr{
font-size:28px;
margin-bottom:65px;
font-weight:bolder;
color:#3d3d3d;
}

.c-testimonial .button-small{
  background: #ffd102;
    color: #002A41;
    padding: 10px 25px;
    border-radius: 10px;
}
</style>

<div class="container">


<section class="c-band c-band--grey c-band--center c-band--testimonials">
  <div class="c-testimonial">
	  <div class="o-container">
      <p class="promo-para">Happy Clients</p>
      <p class="promo-follow">Join the other riders that benefit from ERT</p>
		</div>
      <div class="o-container c-testimonial__wrap">
      <div id="google-reviews" class="c-testimonial-grid">

        <h3 class="gr">5.0 Google rating left by our very own customers. Leave yours <a href="https://www.google.co.uk/search?ei=mQD8W__RB8vSwAKTxrmoBg&q=ert+bike&oq=ert+bike&gs_l=psy-ab.3..0j0i7i10i30j0j0i8i30l4j0i8i10i30.4326.4326..4687...0.0..0.69.69.1......0....1..gws-wiz.......0i71.uXWlpEhabuQ#lrd=0x48763ec9d5aa88a1:0x9d7607569f551d6b,3,,," class="button button-small">Here</a></h3>
        <div class="row row-eq-height">

         <?php 
            $args = array( 
            'post_type' => 'testimonial'
            );
            $the_query = new WP_Query( $args );
        ?>

        <?php 
        if ( $the_query->have_posts() ) :
        while ( $the_query->have_posts() ) : 
        
        $the_query->the_post(); 
        $url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        ?>
          <div class="c-testimonial-grid__item span4">
            <div class="c-testimonial-grid__content">
              <div class="c-testimonial-content">
                <div class="c-testimonial-content__name"><?php echo the_title(); ?></div>
                <div class="c-testimonial-content__time"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' ago ' ; ?></div>
                <div class="stars"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></div>
                <div class="c-testimonial-content__text"><?php echo the_content(); ?></div>
              </div>
            </div>
          </div>
        <?php endwhile; ?> 
        <?php endif; ?>
        <?php wp_reset_query(); ?>

</div>
        <div class="call-to-action-testimonials span12">
          <h3>Start your riding lessons with <strong>ERT</strong> and see what all the fuss is about.  </h3>
          <a href="<?php echo the_permalink('14'); ?>" class="button">Get Started</a>
        </div>



      </div>
        </div>
  </div>
</section>
</div>

<?php get_footer(); ?>
