<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since ERT 1.0
 */

get_header(); ?>
    <div id="primary" class="content-area content_wrapper">
		<main id="main" class="site-main" role="main">
    	<div class="container">
        <div class="content_block no-sidebar row">
          <div class="fl-container span12">    
            <div class="row-fluid">
							<?php
							while ( have_posts() ) : the_post();
					
								get_template_part( 'content', get_post_format() );
										
							endwhile;
							?>
            </div><!-- .row-fluid -->
            <hr class="light">
						<?php get_sidebar(); ?>
          </div><!-- .contentarea -->
        </div>
      </div>
      <div class="clear"><!-- ClearFix --></div>
    </div><!-- .fl-container -->
    <div class="clear"><!-- ClearFix --></div>
  </div>
  </div><!-- .container -->
  </main>
</div><!-- .content_wrapper -->
<?php get_footer(); ?>
