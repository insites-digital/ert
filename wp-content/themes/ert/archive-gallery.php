<?php
/**
 * The template for displaying all single posts and attachments
 * Template Name: Course List
 * @package WordPress
 * @subpackage ERT
 * @since ERT 1.0
 */

get_header(); ?>

<style>
    .c-band--gallery{
        margin-top:120px;
        margin-bottom:110px;
    }
    h4{
        font-weight:bold;
        font-size: 21px;
    }
    .content{
        background: #fafafa;
        padding:15px 20px;
        text-align: left !important;
        
    }
    .lb-data .lb-caption {
    font-size: 13px;
    font-weight: bold;
    line-height: 1em;
    color: #fff;
    font-family: 'Open Sans', serif;
    font-size: 16px;
    line-height: 21px;
}
</style>

<div class="container">

<section class="c-band c-band--grey c-band--center c-band--gallery">
  <div class="c-testimonial">

        <div class="row row-eq-height">

         <?php 
            $args = array( 
            'post_type' => 'gallery'
            );
            $the_query = new WP_Query( $args );
        ?>

        <?php 
        if ( $the_query->have_posts() ) :
        while ( $the_query->have_posts() ) : 
        
        $the_query->the_post(); 
        $url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $content = get_the_content();
        ?>
          <div class="c-testimonial-grid__item span4" data-clearing>
            <a href="<?php echo $url[0] ?>" data-lightbox="image-1" data-title="<?php echo strip_tags($content) ?>"><img src="<?php echo $url[0]; ?>" alt=""></a>
            <div class="content">
                <h4><?php echo the_title(); ?></h4>
                <p><?php echo the_content(); ?></p>
            </div>
          </div>
        <?php endwhile; ?> 
        <?php endif; ?>
        <?php wp_reset_query(); ?>

    </div>


  </div>
</section>
</div>

<?php get_footer(); ?>
